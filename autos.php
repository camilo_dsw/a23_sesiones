<?php

session_start();

// COMPROBAMOS SI LA VARIABLE $_SESSION[] ESTA DEFINIDA Y NO ES NULL

if (isset($_SESSION['email'])) {

  if ($_SESSION['email'] === 'camilo@debiandsw.com') {
    // COMPROBAMOS SI EL USUARIO ESTÁ LOGUEADO
    // SI LO ESTÁ INSTANCIAMOS UN OBJETO DE LA CLASE Auto() y creamos una nueva 
    // conexión con la base de datos

    require "lib/Database.php";
    require "models/Auto.php";
    $obj_Auto = new Auto();
    $obj_Auto->makeConnection();
    $flag_Insertado = false;
    $flag_error = false;
  } else {
    // SI NO TERMINAMOS EL PROGRAMA
    die('Not logged in');
  }
} else {
  die('Not logged in');
}



// comprobamos si se ha definido la variable $_POST[] del formulario donde
// tenemos el botón borrar para eliminar un registro de la base de datos.
// llamamos la función delAuto()

if (isset($_POST['id1'])) {

  $obj_Auto->delAuto($_POST['id']);
}

// comprobamos si están definidos, si no están vacios los campos marca, año y kilometraje
// y si son numéricos los campos año y kilometraje
if (isset($_POST['make']) && isset($_POST['year']) && isset($_POST['milage'])) {


  if (strlen($_POST['make']) > 0 && strlen($_POST['year']) > 0 && strlen($_POST['milage']) > 0) {
    if (is_numeric($_POST['year']) && is_numeric($_POST['milage'])) {


      $obj_Auto->setMake($_POST['make']);
      $obj_Auto->setYear($_POST['year']);
      $obj_Auto->setMileage($_POST['milage']);
      $obj_Auto->addAuto($obj_Auto);

      // añadimos un mensaje para mostrar que se ha añadido un registro a la base de datos.
      $_SESSION["success"] = "Registro insertado.";
      $flag_Insertado = true;
      $flag_error = false;
    } else {

      $_SESSION["success"] = "Kilometraje y año deben ser numéricos";
      $flag_error = true;
    }
  } else {

    $_SESSION["success"] = "Ninguno de los campos pueden estar vacios.";
    $flag_error = true;
    }
    
  }



?>

<?php

require "vistas/autosview.php";
?>

