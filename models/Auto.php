<?php

class Auto{

private $db;



private $make;

private $year;

private $milage;

private $id ;



public function __construct($make = '', $year = 0, $milage = 0)

{

 
  
 $this->make = $make;

 $this->year = $year;

 $this->milage = $milage;

}

//El atributo $db nos servirá para establecer una conexión con la BBDD

public function makeConnection()
{

 $this->db = new Database();

}

// obtener datos de la base de datos
public function getAutos()
{

 $this->db->query("SELECT auto_id, make, year,milage FROM autos");

 $results = $this->db->resultSet();

 return $results;
}


// añadir datos a la base de datos
public function addAuto($auto)
{

 $this->db->query('insert into autos (make,year,milage) Values (:mk,:yr,:mi)');

 $this->db->bind(':mk', $auto->getMake());

 $this->db->bind(':yr', $auto->getYear());

 $this->db->bind(':mi', $auto->getMileage());

 $this->db->execute();

}


public function delAuto($auto)
{
    
    $this->db->query('delete  from autos where auto_id = :id ');
    $this->db->bind(':id', $auto);
    $this->db->execute();
    $auto=0;

}






/**
 * Get the value of make
 */ 
public function getMake()
{
return $this->make;
}

/**
 * Set the value of make
 *
 * @return  self
 */ 
public function setMake($make)
{
$this->make = $make;

return $this;
}

/**
 * Get the value of year
 */ 
public function getYear()
{
return $this->year;
}

/**
 * Set the value of year
 *
 * @return  self
 */ 
public function setYear($year)
{
$this->year = $year;

return $this;
}

 /**
  * Get the value of mileage
  */ 
 public function getMileage()
 {
  return $this->milage;
 } 

 /**
  * Set the value of mileage
  *
  * @return  self
  */ 
 public function setMileage($milage)
 {
  $this->milage = $milage;

  return $this;
 }



public function getId()
{
  return $this ->id;
}




public function setId($id)
{
   $this->id =$id;
   return $this;
}
}


?>