<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login.php</title>

    <link rel="stylesheet" href="estilos.css">
</head>

<body>

    <!-- mostramos un mensaje de registro insertado cuando se mete un registro en la base de datos
la variable booleana $flag_Insertado se pone a true después de llamar a la función 
addauto() que se encarga de añadir los registros a la base de datos-->
    <?php

    if ($flag_Insertado == true) {
        // si a pasado un suceso lo mostramos
        if (isset($_SESSION["success"])) {
            echo ('<p style="color:green">' .htmlentities( $_SESSION["success"]) . "</p>\n");
            // eliminamos los valores que tenga la super variable $_SESSION[]
            unset($_SESSION["success"]);
        }
        // volvemos a poner  a FALSE la variable que nos dice si se ha insertado un registro
        $flag_Insertado = false;
    }
    else
    {
        if ($flag_error == true) {
            // si a pasado un suceso lo mostramos
            if (isset($_SESSION["success"])) {
                echo ('<p style="color:green">' .htmlentities( $_SESSION["success"]) . "</p>\n");
                // eliminamos los valores que tenga la super variable $_SESSION[]
                unset($_SESSION["success"]);
            }
        $flag_error = false;

        }
    }

    ?>


    <form method="post">

        <h3>Inserta una nueva marca</h3>
        </br>
        <p>Nombre marca</p>
        <!-- Los campos son obligatorios -->
        <input type="text" id="marca" name="make" />
        </br>
        <p>Año de compra</p>
        <!-- Los campos son obligatorios -->
        <input type="text" id="ano" name="year" />
        </br>
        <p>Kilometraje</p>
        <input type="text" id="kilome" name="milage" />
        </br>
        </br>
        <input type="submit" value="Insertar" />
        <p><a href="logout.php">Log Out</a></p>

    </form>

</body>

</html>

<?php

// mostramos los registros de la base de datos

$results = $obj_Auto->getAutos();

if ($results > 0) {
  foreach ($results as $result) {
    

    echo "<tr>
        <td> <b>" . $result->auto_id . "</b></td>
        <td><b>" . $result->getMake() . "</b></td>
        <td><b>" . $result->getYear() . "</b></td>
        <td><b>" . $result->getMileage() . "</b></td>
        <td> <form method='post'>
        
        <input type='Submit' name='id1' value ='Borrar' />
        <input type='hidden' name ='id'  value='$result->auto_id'/>
        </form> </td>
        </tr>";
    
  }

  
}

echo ("<div class='colorear'>" . 'Registro insertado' . "</div>");


?>
